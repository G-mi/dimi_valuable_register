/*
dima1894
Dimitrios Mavromatis
Stockholm University
Computer Science & Software Engineering
Spring term 2020
Programming 2
 */

public class Appliance extends Valuable {

    private static final double WEAR_CONVERSION = 0.1;

    private double retailPrice;

    private int wear;

    public Appliance(String name, double retailPrice, int wear)  {
        super(name);

        if (retailPrice < 0) {
            throw new IllegalArgumentException();
        }
        else this.retailPrice = retailPrice;

        if (wear < 0) {
            throw new IllegalArgumentException();
        }
        else this.wear = wear;
    }

    public int getWear() {
        return wear;
    }

    public double getPrice() {
        return retailPrice;
    }

    @Override
    public double getValue() {
        return getPrice() * getWear() * WEAR_CONVERSION;
    }

    @Override
    public String toString() {
        return super.toString().concat(String.format(" condition %d retail price %.2f", getWear(), getPrice()));
    }
}
