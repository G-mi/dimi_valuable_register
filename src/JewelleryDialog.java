/*
dima1894
Dimitrios Mavromatis
Stockholm University
Computer Science & Software Engineering
Spring term 2020
Programming 2
 */

import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class JewelleryDialog extends Alert {
    private TextField nameField = new TextField();
    private TextField jewelsField = new TextField();
    private CheckBox avGuld = new CheckBox();



    public JewelleryDialog() {
        super(AlertType.CONFIRMATION);
        setTitle("New jewellery");
        GridPane grid = new GridPane();
        grid.addRow(0, new Label("Name: "), nameField);
        grid.addRow(1, new Label("Jewels: "), jewelsField);
        grid.addRow(2, new Label("Of gold: "), avGuld);
        getDialogPane().setContent(grid);
        setHeaderText(null);
    }

    public String getName() {
        return nameField.getText();
    }

    public int getJewels() {
        return Integer.parseInt(jewelsField.getText());
    }

    public String getMaterial() {
        if (avGuld.isSelected()) {
            return "guld";
        }
        else return "silver";
    }
}