import java.util.Comparator;

/*
dima1894
Dimitrios Mavromatis
Stockholm University
Computer Science & Software Engineering
Spring term 2020
Programming 2

 */
public abstract class Valuable {

    private final double implicitVatConstant = 0.25;

    private double vat;

    private String name;

    public Valuable(String name) {
        this.name = name;
        setInternalVat(implicitVatConstant);
    }

    private void setInternalVat(double vat) {
        if (vat < 0) {
            throw new IllegalArgumentException();
        } else this.vat = vat;
    }

    public void setVAT(double newVat) {
        setInternalVat(newVat);
    }

    public abstract double getValue();

    public final double getValuePlusVAT() {
        return getValue() + (vat * getValue());
    }

    public final String getName() {
        return name;
    }
    

    public static class NameSorter implements Comparator<Valuable> {

        public int compare(Valuable x, Valuable y) {
            return x.getName().compareToIgnoreCase(y.getName());
        }
    }

    public static class ValueSorter implements Comparator<Valuable> {
        public int compare(Valuable x, Valuable y) {
            return (int)y.getValuePlusVAT() - (int)x.getValuePlusVAT();
        }
    }

    @Override
    public String toString() {
        return String.format("%s, price %.2f (excl VAT %.2f)", getName(), getValuePlusVAT(), getValue());
    }
}