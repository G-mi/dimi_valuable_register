/*
dima1894
Dimitrios Mavromatis
Stockholm University
Computer Science & Software Engineering
Spring term 2020
Programming 2
 */

import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class ApplianceDialog extends Alert {
    private TextField nameField = new TextField();
    private TextField retailPriceField = new TextField();
    private TextField wearField = new TextField();


    public ApplianceDialog() {
        super(AlertType.CONFIRMATION);
        setTitle("New appliance");
        GridPane grid = new GridPane();
        grid.addRow(0, new Label("Name: "), nameField);
        grid.addRow(1, new Label("Price: "), retailPriceField);
        grid.addRow(2, new Label("Condition: "), wearField);
        getDialogPane().setContent(grid);
        setHeaderText(null);
    }

    public String getName() {
        return nameField.getText();
    }


    public double getRetailPrice() {
        return Double.parseDouble(retailPriceField.getText());
    }

    public int getWear() {
        return Integer.parseInt(wearField.getText());
    }
}
