/*
dima1894
Dimitrios Mavromatis
Stockholm University
Computer Science & Software Engineering
Spring term 2020
Programming 2
 */

public class Stock extends Valuable {

    private int quantity;

    private double rate;

    public Stock(String name, int quantity, double rate) {
        super(name);

        if (quantity < 0) {
            throw new IllegalArgumentException();
        } else this.quantity = quantity;

        if (rate < 0) {
            throw new IllegalArgumentException();
        } else setRateInternal(rate);
    }

    private void setRateInternal(double rate) {
        this.rate = rate;
    }

    public void setRate(double newRate) {
        setRateInternal(newRate);
    }

    public double getRate() {
        return rate;
    }

    public int getQuantity() {
        return quantity;
    }

    public void crash() {
        this.rate = 0;
    }

    @Override
    public double getValue() {
        return rate * quantity;
    }

    @Override
    public String toString() {
        return super.toString().concat(String.format(" %.2f rate, %d quantity", rate, quantity));
    }
}