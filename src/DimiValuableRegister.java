/*
dima1894
Dimitrios Mavromatis
Stockholm University
Computer Science & Software Engineering
Spring term 2020
Programming 2
 */

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;


public class DimiValuableRegister extends Application {
    
    private static final int BORDER_SIZE = 7;
    private static final int SCENE_WIDTH = 600;
    private static final int SCENE_HEIGHT = 400;

    private ArrayList<Valuable> allValuables = new ArrayList<>();

    private BorderPane root;
    private VBox right;
    private RadioButton nameButton;
    private TextArea display;
    private MenuButton createValuableButton = new MenuButton("Choose valuable: ");
    private MenuItem jewelleryButton = new MenuItem("Jewellery");
    private MenuItem stockButton = new MenuItem("Stock");
    private MenuItem applianceButton = new MenuItem("Appliance");

    @Override
    public void start(Stage primaryStage) {

        primaryStageSetUp(primaryStage);

        verticalBoxSetUp();

        radioButtonsSetUp();

        textAreaSetUp();

        mainMenuSetUp();

        showSetUp(primaryStage);

    }

    private void showSetUp(Stage primaryStage) {
        Scene scene = new Scene(root,SCENE_WIDTH, SCENE_HEIGHT);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void mainMenuSetUp() {
        createValuableButton.getItems().addAll(jewelleryButton, stockButton, applianceButton);
        jewelleryButton.setOnAction(new JewelleryHandler());
        stockButton.setOnAction(new StockHandler());
        applianceButton.setOnAction(new ApplianceHandler());
        Button showButton = new Button("Show");
        Button crashButton = new Button("Crash");
        FlowPane lowButton = new FlowPane();
        lowButton.setAlignment(Pos.CENTER);
        lowButton.setPadding(new Insets(BORDER_SIZE));
        lowButton.setHgap(BORDER_SIZE);
        root.setBottom(lowButton);
        lowButton.getChildren().addAll(createValuableButton ,showButton, crashButton);
        showButton.setOnAction(new ShowHandler());
        crashButton.setOnAction(new CrashHandler());
    }

    private void textAreaSetUp() {
        display = new TextArea();
        root.setCenter(display);
        display.setEditable(false);
    }

    private void radioButtonsSetUp() {
        nameButton = new RadioButton("Name");
        RadioButton valueButton = new RadioButton("Value");
        right.getChildren().addAll(nameButton, valueButton);
        ToggleGroup group = new ToggleGroup();
        group.getToggles().addAll(nameButton, valueButton);
        nameButton.setSelected(true);
    }

    private void verticalBoxSetUp() {
        right = new VBox();
        right.setPadding(new Insets(BORDER_SIZE));
        right.setSpacing(BORDER_SIZE);
        root.setRight(right);
        Label sortingLabel = new Label("Sort");
        right.getChildren().add(sortingLabel);
    }

    private void primaryStageSetUp(Stage primaryStage) {
        primaryStage.setTitle("Dimi's Valuable Register ");
        root = new BorderPane();
        Label heading = new Label("Valuables");
        FlowPane top = new FlowPane();
        root.setTop(top);
        top.getChildren().add(heading);
        top.setAlignment(Pos.CENTER);
        top.setPadding(new Insets(BORDER_SIZE));
    }


    class JewelleryHandler implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent actionEvent) {
            try {
                JewelleryDialog dialog = new JewelleryDialog();
                Optional<ButtonType> answer = dialog.showAndWait();
                if (answer.isPresent() && answer.get() == ButtonType.OK) {
                    String name = dialog.getName();
                    if (name.isEmpty()) {
                        Alert alert = new Alert(Alert.AlertType.ERROR, "Name field cannot be empty!");
                        alert.showAndWait();
                        return;
                    }
                    int jewels = dialog.getJewels();
                    String material = dialog.getMaterial();

                    Jewellery newJewellery = new Jewellery(name, jewels, material);
                    allValuables.add(newJewellery);
                }
            } catch (NumberFormatException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR, "Provide a number!");
                alert.showAndWait();
            }
        }
    }

    class StockHandler implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent actionEvent) {
            try {
                StockDialog dialog = new StockDialog();
                Optional<ButtonType> answer = dialog.showAndWait();
                if (answer.isPresent() && answer.get() == ButtonType.OK) {
                    String name = dialog.getName();
                    if (name.isEmpty()) {
                        Alert alert = new Alert(Alert.AlertType.ERROR, "Name field cannot be empty!");
                        alert.showAndWait();
                        return;
                    }
                    int quantity = dialog.getQuantity();
                    double rate = dialog.getRate();

                    Stock newStock = new Stock(name, quantity, rate);
                    allValuables.add(newStock);
                }
            } catch (NumberFormatException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR, "Provide a number!");
                alert.showAndWait();
            }
        }
    }

    class ApplianceHandler implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent actionEvent) {
            try {
                ApplianceDialog dialog = new ApplianceDialog();
                Optional<ButtonType> answer = dialog.showAndWait();
                if (answer.isPresent() && answer.get() == ButtonType.OK) {
                    String name = dialog.getName();
                    if (name.isEmpty()) {
                        Alert alert = new Alert(Alert.AlertType.ERROR, "Name field cannot be empty!");
                        alert.showAndWait();
                        return;
                    }
                    double retailPrice = dialog.getRetailPrice();
                    int condition = dialog.getWear();

                    Appliance newAppliance = new Appliance(name, retailPrice, condition);
                    allValuables.add(newAppliance);
                }
            } catch (NumberFormatException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR, "Provide a number!");
                alert.showAndWait();
            }
        }
    }

    class ShowHandler implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent actionEvent) {
            display.clear();
            if(nameButton.isSelected()) {
                ArrayList<Valuable> sortedByName = new ArrayList<>(allValuables);
                Collections.sort(sortedByName, new Valuable.NameSorter());
                for (Valuable v : sortedByName) {
                    display.appendText(v.toString() + "\n");
                }
            }
            else {
                ArrayList<Valuable> sortedByName = new ArrayList<>(allValuables);
                Collections.sort(sortedByName, new Valuable.ValueSorter());
                for (Valuable v : sortedByName) {
                    display.appendText(v.toString() + "\n");
                }
            }
        }
    }

    class CrashHandler implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent actionEvent) {
            for (Valuable v : allValuables) {
                if (v instanceof Stock) {
                    ((Stock) v).crash();
                }
            }
        }
    }
}
