/*
dima1894
Dimitrios Mavromatis
Stockholm University
Computer Science & Software Engineering
Spring term 2020
Programming 2
 */

public class Jewellery extends Valuable {

    private final int jewelUnitValue = 500;

    private final Material material;

    private int numberOfJewels;

    public Jewellery(String name, int numberOfJewels, String material) {
        super(name);

        if (numberOfJewels < 0) {
            throw new IllegalArgumentException();
        }
        else this.numberOfJewels = numberOfJewels;

        if (material.equalsIgnoreCase("guld")) {
            this.material = Material.Guld;
        }
        else if (material.equalsIgnoreCase("silver")) {
            this.material = Material.Silver;
        }
        else throw new IllegalArgumentException();
    }

    public int getJewels() {
        return numberOfJewels;
    }

    public Material getMaterial() {
        return material;
    }

    @Override
    public double getValue() {
        return material.getValue() + jewelUnitValue * getJewels();
    }

    @Override
    public String toString() {
        return super.toString().concat(String.format(" %s %s material %d Jewels", getName(), getMaterial(), getJewels()));
    }
}