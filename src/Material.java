/*
dima1894
Dimitrios Mavromatis
Stockholm University
Computer Science & Software Engineering
Spring term 2020
Programming 2
 */

public enum Material {

    Guld(2000), Silver(700);

    private final int value;

    Material(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
